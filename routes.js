const express = require('express')
var router = express.Router();
var moviesController = require('./controllers/moviesController');


/* router.get('/api/directors', (req, res) => res.send(movies)) */
router.get('/api/movies', moviesController.index);
router.get('/api/movies/:id', moviesController.show);

// Delete a Customer with Id
router.delete('/api/movies/:id', moviesController.deleteMovie);

module.exports = router;